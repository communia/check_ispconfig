import logging
import os
from pathlib import Path
import ruamel.yaml
import sys
from ruamel.yaml.compat import StringIO


logger = logging.getLogger(__name__)

class Setter:
    def __init__(self):
        self.yaml = ruamel.yaml.YAML()

    def load_configuration(self, filename):
        '''
        Load the given configuration file and return its contents as a data structure of nested dicts
        and lists.
        Raise ruamel.yaml.error.YAMLError if something goes wrong parsing the YAML, or RecursionError
        if there are too many recursive includes.
        '''
        # Load with coments
        inf = Path(filename)
        self.yaml.Constructor = Include_constructor_rt
        data = self.yaml.load(inf)
        #yaml.dump(data, sys.stdout)
        return data 
        # Load safe without comments
        self.yaml = ruamel.yaml.YAML(typ='safe')
        self.yaml.Constructor = Include_constructor
        return self.yaml.load(open(filename))

    def conf_dump(self, data):
        #yaml = ruamel.yaml.YAML()
        #yaml.dump(data, ruamel.yaml.RoundTripDumper)
        #return
        #yaml.Constructor = Include_constructor
        return self.yaml.dump(data, sys.stdout)
        ruamel.yaml.round_trip_dump(data, sys.stdout)


    def configuration_dump(self, data, stream=None, **kw):
        inefficient = False
        if stream is None:
            inefficient = True
            stream = StringIO()
        self.yaml.YAML.dump(data, stream, **kw)
        if inefficient:
            return stream.getvalue()

    def include_configuration(self, loader, filename_node):
        '''
        Load the given YAML filename (ignoring the given loader so we can use our own), and return its
        contents as a data structure of nested dicts and lists.
        '''
        return self.load_configuration(os.path.expanduser(filename_node.value))


def load_configuration(filename):
    '''
    Load the given configuration file and return its contents as a data structure of nested dicts
    and lists.
    Raise ruamel.yaml.error.YAMLError if something goes wrong parsing the YAML, or RecursionError
    if there are too many recursive includes.
    '''
    # Load with coments
    inf = Path(filename)
    yaml = ruamel.yaml.YAML()
    yaml.Constructor = Include_constructor_rt
    data = yaml.load(inf)
    #yaml.dump(data, sys.stdout)
    return data 
    # Load safe without comments
    yaml = ruamel.yaml.YAML(typ='safe')
    yaml.Constructor = Include_constructor
    return yaml.load(open(filename))

def conf_dump(data):
    yaml = ruamel.yaml.YAML()
    #yaml.dump(data, ruamel.yaml.RoundTripDumper)
    #return
    #yaml.Constructor = Include_constructor
    return yaml.dump(data, sys.stdout)
    ruamel.yaml.round_trip_dump(data, sys.stdout)


def configuration_dump(data,  stream=None, **kw):
    inefficient = False
    if stream is None:
        inefficient = True
        stream = StringIO()
    ruamel.yaml.YAML.dump(data, stream, **kw)
    if inefficient:
        return stream.getvalue()

def include_configuration(loader, filename_node):
    '''
    Load the given YAML filename (ignoring the given loader so we can use our own), and return its
    contents as a data structure of nested dicts and lists.
    '''
    return load_configuration(os.path.expanduser(filename_node.value))

class Include_constructor_rt(ruamel.yaml.RoundTripConstructor):
    '''
    Constructor with comments
    A YAML "constructor" (a ruamel.yaml concept) that supports a custom "!include" tag for including
    separate YAML configuration files. Example syntax: `retention: !include common.yaml`
    '''

    def __init__(self, preserve_quotes=None, loader=None):
        super(Include_constructor_rt, self).__init__(preserve_quotes, loader)
        # We are just tweaking data to include servers, dont' care about external includes :( or
        # death recursion due representer.represent_data depth and depth ... so: Disable 
        # self.add_constructor('!include', include_configuration)

    def flatten_mapping(self, node):
        '''
        Support the special case of shallow merging included configuration into an existing mapping
        using the YAML '<<' merge key. Example syntax:
        ```
        retention:
            keep_daily: 1
            <<: !include common.yaml
        ```
        '''
        return
        representer = ruamel.yaml.representer.RoundTripRepresenter()

        for index, (key_node, value_node) in enumerate(node.value):
            if key_node.tag == u'tag:yaml.org,2002:merge' and value_node.tag == '!include':
                included_value = representer.represent_data(self.construct_object(value_node))
                node.value[index] = (key_node, included_value)

        super(Include_constructor_rt, self).flatten_mapping(node)

'''
class with no comments (fast and safe)
'''
class Include_constructor(ruamel.yaml.SafeConstructor):
    '''
    A YAML "constructor" (a ruamel.yaml concept) that supports a custom "!include" tag for including
    separate YAML configuration files. Example syntax: `retention: !include common.yaml`
    '''

    def __init__(self, preserve_quotes=None, loader=None):
        super(Include_constructor, self).__init__(preserve_quotes, loader)
        self.add_constructor('!include', include_configuration)

    def flatten_mapping(self, node):
        '''
        Support the special case of shallow merging included configuration into an existing mapping
        using the YAML '<<' merge key. Example syntax:
        ```
        retention:
            keep_daily: 1
            <<: !include common.yaml
        ```
        '''
        representer = ruamel.yaml.representer.SafeRepresenter()

        for index, (key_node, value_node) in enumerate(node.value):
            if key_node.tag == u'tag:yaml.org,2002:merge' and value_node.tag == '!include':
                included_value = representer.represent_data(self.construct_object(value_node))
                node.value[index] = (key_node, included_value)

        super(Include_constructor, self).flatten_mapping(node)


