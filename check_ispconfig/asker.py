import logging
import os
import mysql.connector
from mysql.connector import Error

logger = logging.getLogger(__name__)

class Asker: 
    def select(self, query, query_args):
        for hostname in self.mysql_config:
            server = self.mysql_config[hostname]
            try:
                connection = mysql.connector.connect(**server)
                cursor = connection.cursor(dictionary=True)
                cursor.execute(query, query_args)
                records = cursor.fetchall()
                return records
                # TBD other infos
                #print("Total number: ", cursor.rowcount)
                #for row in cursor:
                #    print("* {Name}".format(Name=row['Name']
                #for row in cursor:
                #    print("* {Name}: {Population}".format(**row))
                #for row in records:
                #    print("Id = ", row[0], )
                #    print("Name = ", row[1])
                #    print("Price  = ", row[2])
                #    print("Purchase date  = ", row[3], "\n")

            except Error as e:
                print("Error reading data from MySQL table", e)
            finally:
                if (connection.is_connected()):
                    connection.close()
                    cursor.close()
    def list_active_dbs(self, re_exclude_domain="^[.*]"):
        # Could be also backup if client.canceled=n client.locked=n, and  web_domain.active=y
        return self.select("SELECT database_name,server_name FROM `web_database` LEFT JOIN `server` ON web_database.server_id = server.server_id LEFT JOIN `web_domain` ON `web_domain`.domain_id = web_database.parent_domain_id WHERE web_database.active LIKE 'y' AND web_domain.domain NOT REGEXP %s ORDER BY server_name", (re_exclude_domain,))
    def list_inactive_dbs(self, re_exclude_domain="^[.*]"):
        return self.select("SELECT database_name,server_name FROM `web_database` LEFT JOIN `server` ON web_database.server_id = server.server_id LEFT JOIN `web_domain` ON `web_domain`.domain_id = web_database.parent_domain_id WHERE web_database.active LIKE 'n' AND web_domain.domain NOT REGEXP %s ORDER BY server_name", (re_exclude_domain,))
    def list_active_web_domain_sites(self, re_exclude_domain="^[.*]"):
        return self.select("SELECT document_root,server_name,domain,`ssl`,`ssl_letsencrypt` FROM `web_domain` LEFT JOIN `server` ON web_domain.server_id = server.server_id WHERE web_domain.active LIKE 'y' AND web_domain.domain NOT REGEXP %s AND NULLIF(web_domain.redirect_path, '') IS NULL  ORDER BY server_name", (re_exclude_domain,))
    def list_inactive_web_domain_sites(self, re_exclude_domain="^[.*]"):
        return self.select("SELECT document_root,server_name,domain,`ssl`,`ssl_letsencrypt` FROM `web_domain` LEFT JOIN `server` ON web_domain.server_id = server.server_id WHERE web_domain.active LIKE 'n' AND web_domain.domain NOT REGEXP %s ORDER BY server_name", (re_exclude_domain,))
    def __init__(self,mysql_config):
        self.mysql_config = mysql_config
